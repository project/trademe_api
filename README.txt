 TRADEME API MODULE FOR DRUPAL 6.x
 ---------------------------------

 CONTENTS OF THIS README
 -----------------------
   * Description
   * Features
   * TODO
   * Requirements
   * Examples
      * Getting a request token
      * Generating an authorisation URL
      * Getting an access token
      * Making a single API query
      * Making an aggregate API query
      * The test page


 DESCRIPTION
 -----------
 This module hopes to serve as the base for any application that wants to make
 use of the Trade Me API. It hopes to make the process of quering Trade Me's
 API easier and to help off load things like caching behind the scenes.


 FEATURES
 --------
 * Provides wrapping methods for the Drupal OAuth module (which in turn provides
   wrapping methods for the PHP OAuth library), which helps abstract most
   of the hard work of OAuth
 * Re-uses the Drupal OAuth module methods where appropriate
 * Re-uses the Drupal OAuth database schema
 * Makes use of Drupal cache_set() for caching of API queries
 * Can cache aggregate API queries
 * Configurable cache expiry
 * Configurable OAuth application keys for your Trade Me application
 * Makes it easy to query most of the Trade Me API methods
 * Has internal objects for
    * Trade Me Item
    * Trade Me Person
 * All timestamps within the above Trade Me objects are converted to UNIX
   timestamps for easy formatting within drupal


 TODO
 ----
 * Implement POST API methods to write data back to Trade Me (currently only
   GET methods are supported - read only) (patches welcome)
 * See inline comments in the code for TODO's (patches welcome)
 * Implement a statistics page and possibly API to allow easy presentation of
   the uptake of the API on your site
 * Proxy support for cURL (can still be done through setting global config
   options for cURL


 REQUIREMENTS
 ------------
 * You are using version 6.x-3.0-beta3 of the Drupal Oauth module
   http://drupal.org/project/oauth - this is because beta4 introduced a number
   of API changes that as yet do not work this this module. File is at
   http://ftp.drupal.org/files/projects/oauth-6.x-3.0-beta3.tar.gz
 * You have registered an application with Trade Me at
   http://developer.trademe.co.nz/api-overview/registering-an-application/
   - and you have got the consumer key and secret
 * You have the cURL library installed on your web server
 * cURL has firewall access to (these can represent a lot of IP addresses):
    * https://api.trademe.co.nz
    * https://secure.trademe.co.nz


 INSTALLATION
 ------------
 * Install the module as per any other drupal module
 * Visit 'admin/settings/trademe_api' and configure the module, and make sure
   to click 'Save'. By clicking save you are writing the consumer object to the
   database.
 * Write you own module/theme that implements the below methods


 EXAMPLE - GETTING A REQUEST TOKEN
 ---------------------------------
 <?php
   $consumer_token = variable_get('trademe_api_tokens_consumer_key', '');
   $consumer = oauth_common_consumer_consumer_load($consumer_token);
   $request_token = trademe_api_create_request_token($consumer, $user);
 ?>


 EXAMPLE - GENERATING AN AUTHORISATION URL
 -----------------------------------------
 <?php
   $trademe_authorisation_url = trademe_api_authorize_url($consumer, $request_token);
 ?>


 EXAMPLE - GETTING AN ACCESS TOKEN
 ---------------------------------
 This is done for you automatically upon successful authorisation


 EXAMPLE - MAKING A SINGLE API QUERY
 -----------------------------------
 <?php
   global $user;
   $access_tokens = oauth_common_get_user_tokens($user->uid, 'access', 0);
   $trademe_items = trademe_api_get_query('/MyTradeMe/SoldItems.xml', $access_tokens[0]);
 ?>

 You can now iterate of the array of Trade Me items returned and display them
 for example


 EXAMPLE - MAKING AN AGGREGATE API QUERY
 ---------------------------------------
 <?php
   global $user;
   $access_tokens = oauth_common_get_user_tokens($user->uid, 'access', 0);
   $trademe_items = trademe_api_get_query(
     array(
       '/MyTradeMe/SoldItems.xml',
       '/MyTradeMe/Won.xml',
     ),
     $access_tokens[0]
   );
 ?>

 You can now iterate of the array of Trade Me items returned and display them
 for example

 EXAMPLE - THE TEST PAGE
 -----------------------
 Baked into this module is a test page, that you can use to see OAuth in action
 and see two API queries returning (recently sold and won items). Visit

 /admin/settings/trademe_api/test

 to see it in action
