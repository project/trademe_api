<?php

/**
 * @file
 * Class file for TradeMeAttribute
 */

/**
 * Class to describe an attribute of a Trade Me Item on Trade Me. This class
 * contains a lot of attributes, some of them are optional for a given attribute
 */
class TradeMeAttribute {

  // defaults - cannot be NULL
  public $Name = '';
  public $DisplayName = '';
  public $Value = '';
  public $Attribute = '';

  public function __construct($item = NULL) {

    // loop through each attribute and assign to the object
    foreach ($item->children() as $attribute) {
      $name = $attribute->getName();
      $value = (string) $attribute;

      // only assign the attribute if it is available
      if (isset($this->{$name})) {
        // store booleans as such
        if ($value == 'true') {
          $value = (bool) TRUE;
        }
        elseif ($value == 'false') {
          $value = (bool) FALSE;
        }

        // special cases for timestamps, store them as epoch so drupal can
        // format them for display easier later on down the track
        if (drupal_substr($name, 0, 4) == 'Date' || $name == 'AsAt') {
          $this->{$name} = strtotime($value);
        }
        // no special case reached, just store the value verbatim
        else {
          $this->{$name} = $value;
        }
      }
      else {
        watchdog('trademe_api', 'Failed to store the attribute: %name with value: %value in object: %object - please submit patch to address this', array(
          '%name' => $name,
          '%value' => $value,
          '%object' => get_class($this),
        ));
      }
    }
  }

  // prettier toString()
  public function __toString() {
    return $this->DisplayName . ' = ' . $this->Value;
  }
}
