<?php

/**
 * @file
 * Class file for TradeMeItem
 */

/**
 * Class to descibe an item for sale on trademe. This class contains a lot of
 * attributes, some of them are optional for a given auction
 */
class TradeMeItem {

  // defaults - cannot be NULL
  public $ListingId = '';
  public $Title = '';
  public $Category = '';
  public $StartPrice = '';
  public $StartDate = 0;
  public $EndDate = 0;
  public $ListingLength = '';
  public $AsAt = 0;
  public $CategoryPath = '';
  public $PictureHref = '';
  public $PhotoId = '';
  public $Seller = '';
  public $Buyer = '';
  public $Note = '';
  public $NoteId = 0;
  public $NoteDate = 0;
  public $CategoryName = '';
  public $ReserveState = '';
  public $IsClassified = FALSE;
  public $AuctionAttribute = '';
  public $BuyerFeedbackPlaced = FALSE;
  public $SellerFeedbackPlaced = FALSE;
  public $DeliveryId = 0;
  public $FpoDecisionViaMobile = FALSE;
  public $HasPaidByCreditCard = FALSE;
  public $InvoiceId = 0;
  public $OfferId = 0;
  public $PaymentInfo = FALSE;
  public $Price = 0.00;
  public $ShippingPrice = 0;
  public $SoldDate = 0;
  public $SoldType = '';
  public $IsDailyDeal = FALSE;
  public $BuyNowPrice = 0;
  public $Restrictions = 0;
  public $BidderAndWatchers = 0;
  public $MaxBidAmount = 0;
  public $BidCount = 0;
  public $ViewCount = 0;
  public $IsReserveMet = FALSE;
  public $HasReserve = FALSE;
  public $ReservePrice = 0;
  public $SalePrice = 0;
  public $SelectedShipping = FALSE;
  public $BuyerDeliveryAddress = '';
  public $MessageFromBuyer = '';
  public $SuccessFees = 0;
  public $IsPayNowPurchase = FALSE;
  public $HasSellerPlaceFeedback = FALSE;
  public $HasBuyerPlaceFeedback = FALSE;
  public $DeliveryAddress = '';
  public $HasGallery = FALSE;
  public $IsNew = FALSE;
  public $Status = '';
  public $StatusDate = 0;
  public $HasPayNow = FALSE;
  public $HasScheduledEndDate = FALSE;
  public $Attributes = '';
  public $Attribute = '';

  // default constructor
  public function __construct($item = NULL) {

    // loop through each attribute and assign to the object
    foreach ($item->children() as $attribute) {

      $name = $attribute->getName();
      $value = (string) $attribute;

      // only assign the attribute if it is available
      if (isset($this->{$name})) {

        // store booleans as such
        if ($value == 'true') {
          $value = (bool) TRUE;
        }
        elseif ($value == 'false') {
          $value = (bool) FALSE;
        }

        // special cases for timestamps, store them as epoch so drupal can
        // format them for display easier later on down the track
        if (drupal_substr($name, -4, 4) == 'Date' || $name == 'AsAt') {
          $this->{$name} = strtotime($value);
        }
        // special case for the seller - use an object, also set buyer to NULL
        elseif ($name == 'Seller') {
          require_once('TradeMePerson.inc');
          $this->{$name} = new TradeMePerson($attribute);
          $this->Buyer = NULL;
        }
        // special case for the buyer - use an object, also set seller to NULL
        elseif ($name == 'Buyer') {
          require_once('TradeMePerson.inc');
          $this->{$name} = new TradeMePerson($attribute);
          $this->Seller = NULL;
        }
        elseif ($name == 'Attributes') {
          require_once('TradeMeAttribute.inc');
          $this->{$name} = new TradeMeAttribute($attribute);
        }
        // no special case reached, just store the value verbatim
        else {
          $this->{$name} = $value;
        }
      }
      else {
        watchdog('trademe_api', 'Failed to store the attribute: %name with value: %value in object: %object - please submit patch to address this', array(
          '%name' => $name,
          '%value' => $value,
          '%object' => get_class($this),
        ));
      }
    }
  }

  // prettier toString()
  public function __toString() {
    return $this->Title . ' (#' . $this->ListingId . ')';
  }
}
