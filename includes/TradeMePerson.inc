<?php

/**
 * @file
 * Class file for TradeMePerson
 */

/**
 * Class to descibe a person (trader) on trademe. This class contains a lot of
 * attributes, some of them are optional for a given person
 */
class TradeMePerson {

  // defaults - cannot be NULL
  public $MemberId = 0;
  public $Nickname = '';
  public $DateAddressVerified = 0;
  public $DateJoined = 0;
  public $Email = '';
  public $UniqueNegative = 0;public $NegativeFeedback = 0;
  public $UniquePositive = 0;public $PositiveFeedback = 0;
  public $FeedbackCount = 0;public $TotalFeedback = 0;
  public $IsAddressVerified = FALSE;
  public $IsDealer = FALSE;
  public $WatchListCount = 0;
  public $WonCount = 0;
  public $LostCount = 0;
  public $SellingCount = 0;
  public $SoldCount = 0;
  public $UnsoldCount = 0;
  public $Balance = 0;
  public $PayNowBalance = 0;
  public $FirstName = '';
  public $LastName = '';
  public $FixedPriceOffers = 0;

  // generated items
  public $url = '';

  public function __construct($person = NULL) {

    // loop through each attribute and assign to the object
    foreach ($person->children() as $attribute) {
      $name = $attribute->getName();
      $value = (string) $attribute;

      // only assign the attribute if it is available
      if (isset($this->{$name})) {
        // store booleans as such
        if ($value == 'true') {
          $value = (bool) TRUE;
        }
        elseif ($value == 'false') {
          $value = (bool) FALSE;
        }

        // special cases for timestamps, store them as epoch so drupal can
        // format them for display easier later on down the track
        if (drupal_substr($name, 0, 4) == 'Date' || $name == 'AsAt') {
          $this->{$name} = strtotime($value);
        }
        // no special case reached, just store the value verbatim
        else {
          $this->{$name} = $value;
        }
      }
      else {
        watchdog('trademe_api', 'Failed to store the attribute: %name with value: %value in object: %object - please submit patch to address this', array(
          '%name' => $name,
          '%value' => $value,
          '%object' => get_class($this),
        ));
      }
    }

    // compute some elements
    $this->url = 'http://www.trademe.co.nz/Members/Profile.aspx?member=' . $this->MemberId;
  }

  // prettier toString()
  public function __toString() {
    return $this->Nickname . ' (#' . $this->MemberId . ')';
  }
}
