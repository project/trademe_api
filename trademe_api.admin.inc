<?php

/**
 * Form builder; Displays trademe_api_admin_setting's configuration
 * page.
 *
 * @see system_settings_form()
 */
function trademe_api_admin_settings() {
  $form = array();

  $form['trademe_api_name'] = array(
    '#type'          => 'textfield',
    '#size'          => 24,
    '#default_value' => variable_get('trademe_api_name', 'Trade Me API'),
    '#title'         => t('Name'),
    '#description'   => t('A unique machine-readable name used to identify this provider internally. It may only contain lowercase alphanumeric characters and underscores. No spaces or uppercase characters.'),
    '#required'      => TRUE,
  );

  $form['trademe_api_url_authentication'] = array(
    '#type'        => 'textfield',
    '#title'       => t('Base authentication URL'),
    '#description' => t('A URL to the OAuth provider that is used for authentication. Leave a trailing slash.'),
    '#size'        => 32,
    '#maxlength'   => 255,
    '#required'    => TRUE,
    '#default_value' => variable_get('trademe_api_url_authentication', 'https://secure.trademe.co.nz/'),
  );

  $form['trademe_api_url_query'] = array(
    '#type'        => 'textfield',
    '#title'       => t('Base query URL'),
    '#description' => t('A URL to the OAuth provider that is used for API queries. Leave a trailing slash.'),
    '#size'        => 32,
    '#maxlength'   => 255,
    '#required'    => TRUE,
    '#default_value' => variable_get('trademe_api_url_query', 'https://api.trademe.co.nz/'),
  );

  // version of the API to use
  $form['trademe_api_version'] = array(
    '#type'        => 'select',
    '#title'       => t('Version'),
    '#description' => t('The version of the trademe API to use, currently only one version has been released.'),
    '#options'     => array(
      'v1' => t('version 1'),
    ),
    '#required'    => TRUE,
    '#default_value' => variable_get('trademe_api_version', 'v1'),
  );

  // tokens fieldset
  $form['trademe_api_tokens'] = array(
    '#title' => t('OAuth Tokens'),
    '#type' => 'fieldset',
    '#description' => t('Use these options to specify the secret tokens for accessing Trade Me via OAuth.'),
  );

  // consumer key
  $form['trademe_api_tokens']['trademe_api_tokens_consumer_key'] = array(
    '#type'          => 'textfield',
    '#title'         => t('OAuth Consumer Key'),
    '#description'   => t('Your consumer key provided by the OAuth provider.'),
    '#required'      => TRUE,
    '#size'          => 32,
    '#maxlength'     => 255,
    '#default_value' => variable_get('trademe_api_tokens_consumer_key', ''),
  );

  // consumer secret
  $form['trademe_api_tokens']['trademe_api_tokens_consumer_secret'] = array(
    '#type'          => 'textfield',
    '#title'         => t('OAuth Consumer Secret'),
    '#description'   => t('Your consumer secret provided by the OAuth provider.'),
    '#required'      => TRUE,
    '#size'          => 32,
    '#maxlength'     => 255,
    '#default_value' => variable_get('trademe_api_tokens_consumer_secret', ''),
  );

  // caching fieldset
  $form['trademe_api_caching'] = array(
    '#title' => t('OAuth Caching'),
    '#type' => 'fieldset',
    '#description' => t('Use these options to specify the caching options Trade Me via OAuth.'),
  );

  // caching seconds
  $form['trademe_api_caching']['trademe_api_caching_length'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Seconds'),
    '#description'   => t('The length of time to cache results for. Longer periods are better for performance, but the data may get stale.'),
    '#required'      => TRUE,
    '#size'          => 4,
    '#maxlength'     => 6,
    '#default_value' => variable_get('trademe_api_caching_length', 300),
  );

  // advanced options fieldset
  $form['trademe_api_advanced'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('OAuth Consumer Advanced Settings'),
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
  );

  // hash algorithms - currently on HMAC-SHA1 is supported
  // @see http://developer.trademe.co.nz/api-overview/authentication-authorisation/oauth/
  $sign_methods = array(
    'HMAC-SHA1' => 'HMAC-SHA1',
  );
  $form['trademe_api_advanced']['trademe_api_advanced_signature_method'] = array(
    '#type'          => 'select',
    '#title'         => t('Signature method'),
    '#description'   => t('Trade Me at present only supports one method for signing - HMAC-SHA1'),
    '#options'       => $sign_methods,
    '#required'      => TRUE,
    '#default_value' => variable_get('trademe_api_advanced_signature_method', 'HMAC-SHA1'),
  );

  // authentication realm
  // @see http://tools.ietf.org/html/rfc2617
  $form['trademe_api_advanced']['trademe_api_advanced_authentication_realm'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Authentication realm'),
    '#description'   => t('This is used in HTTP authentication, it is optional.'),
    '#size'          => 32,
    '#maxlength'     => 255,
    '#default_value' => variable_get('trademe_api_advanced_authentication_realm', ''),
  );

  // request token endpoint
  // @see http://developer.trademe.co.nz/api-documentation/authentication-methods/request-token/
  $form['trademe_api_advanced']['trademe_api_advanced_request_token_endpoint'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Request token endpoint'),
    '#description'   => t('Absolute path or path relative to base URL. Most likely will not need to change unless Trade Me change it\'s endpoints.'),
    '#size'          => 32,
    '#maxlength'     => 255,
    '#required'      => TRUE,
    '#default_value' => variable_get('trademe_api_advanced_request_token_endpoint', 'Oauth/RequestToken'),
  );

  // authorisation endpoint
  // @see http://developer.trademe.co.nz/api-documentation/authentication-methods/authorize/
  $form['trademe_api_advanced']['trademe_api_advanced_authorization_endpoint'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Authorization endpoint'),
    '#description'   => t('Absolute path or path relative to base URL. Most likely will not need to change unless Trade Me change it\'s endpoints.'),
    '#size'          => 32,
    '#maxlength'     => 255,
    '#required'      => TRUE,
    '#default_value' => variable_get('trademe_api_advanced_authorization_endpoint', 'Oauth/Authorize'),
  );

  // access token endpoint
  // @see http://developer.trademe.co.nz/api-documentation/authentication-methods/access-token/
  $form['trademe_api_advanced']['trademe_api_advanced_access_token_endpoint'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Access token endpoint'),
    '#description'   => t('Absolute path or path relative to base URL. Most likely will not need to change unless Trade Me change it\'s endpoints.'),
    '#size'          => 32,
    '#maxlength'     => 255,
    '#required'      => TRUE,
    '#default_value' => variable_get('trademe_api_advanced_access_token_endpoint', 'Oauth/AccessToken'),
  );

  // callback URL
  $form['trademe_api_advanced']['trademe_api_advanced_callback_url'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Callback URL'),
    '#description'   => t('Either \'oob\' (out of band), or an absolute path to the URL to redirect users to after successful authentication and authorisation. Do not modify this value unless you know what you are doing.'),
    '#size'          => 32,
    '#maxlength'     => 255,
    '#required'      => TRUE,
    '#default_value' => variable_get('trademe_api_advanced_callback_url', trademe_api_callback_url()),
  );

  // redirection URL
  $form['trademe_api_advanced']['trademe_api_advanced_application_url'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Application URL'),
    '#description'   => t('An absolute or relative path to the URL to redirect users to after successful callback and access token generation - typically your application'),
    '#size'          => 32,
    '#maxlength'     => 255,
    '#required'      => TRUE,
    '#default_value' => variable_get('trademe_api_advanced_application_url', url('<front>', array('absolute' => TRUE))),
  );

  // custom validation function to write the consumer settings into the
  // database
  $form['#validate'][] = 'trademe_api_provider_save';

  return system_settings_form($form);
}

