<?php

/**
 * @file
 * Demonstration page for the Trade Me API
 */

/**
 * Simple testing page for the Trade Me API, performs two API queries to gather
 * your won and sold items and display them in a table
 *
 * You  tokens associated to your current user ID are also displayed
 */
function trademe_api_test() {
  global $user;

  // check for tokens already there
  $request_tokens = oauth_common_get_user_tokens($user->uid, 'request', 0);
  $access_tokens = oauth_common_get_user_tokens($user->uid, 'access', 0);
  $content = '<h2>' . t('Oauth tokens') . '</h2>';

  // no tokens for you current logged in user, get request token and create
  // authorise URL to trademe
  if (count($access_tokens) == 0 && count($request_tokens) == 0) {
    // first get request token from TradeMe, and form the URL. Null's must be
    // passed in to allow the consumer object to set the URL.
    //
    // Could not use the function oauth_common_get_request_token() as it sets
    // the default callback to 'oob' (out of band), and instead we want them
    // to be redirected back to the consumer application

    // get the consumer token
    $consumer_token = variable_get('trademe_api_tokens_consumer_key', '');

    // load the consumer object
    $consumer = oauth_common_consumer_consumer_load($consumer_token);
    $request_token = trademe_api_create_request_token($consumer, $user);

    // try getting the request tokens again
    $request_tokens = oauth_common_get_user_tokens($user->uid, 'request', 0);
  }

  // display the token table
  if (count($access_tokens) > 0 || count($request_tokens) > 0) {
    $content .= drupal_get_form('trademe_api_token_list', $user->uid);
  }

  // test the API if you have an access token
  if (count($access_tokens) > 0) {
    // get a list of recent sold items
    $trademe_items = trademe_api_get_query('/MyTradeMe/Won.xml', $access_tokens[0]);
    if (count($trademe_items) > 0) {
      $content .= '<h2>' . t('Won items') . '</h2>';

      // build a table fo show some information
      $rows = array();
      foreach ($trademe_items as $item) {
        $rows[] = array('<strong><a href="http://www.trademe.co.nz/' . $item->ListingId . '">' . $item . '</a></strong>', '<img src="' . $item->PictureHref . '"/>');
      }
      $header = array(t('Title'), t('Image'));
      $content .= theme('table', $header, $rows);
    }

    // get a list of recent sold items
    $trademe_items = trademe_api_get_query('/MyTradeMe/SoldItems.xml', $access_tokens[0]);
    if (count($trademe_items) > 0) {
      $content .= '<h2>' . t('Sold items') . '</h2>';

      // build a table fo show some information
      $rows = array();
      foreach ($trademe_items as $item) {
        $rows[] = array('<strong><a href="http://www.trademe.co.nz/' . $item->ListingId . '">' . $item . '</a></strong>', '<img src="' . $item->PictureHref . '"/>');
      }
      $header = array(t('Title'), t('Image'));
      $content .= theme('table', $header, $rows);
    }
  }

  return $content;
}
