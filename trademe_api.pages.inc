<?php
/**
 * @file
 * OAuth client library for the TradeMe API
 */

/**
 * Callback authorized token
 *
 * Warning: we just mark the token as authorized without further checking.
 *
 * @TODO: Check verification code
 */
function trademe_api_authorize_callback() {

  $req = DrupalOAuthRequest::from_request();
  $token_key = $req->get_parameter('oauth_token');

  if ($token = DrupalOAuthToken::load($token_key, FALSE)) {
    $oauth_verifier = $req->get_parameter('oauth_verifier');
    $consumer = DrupalOAuthConsumer::load($token->consumer_key, FALSE);
  }

  if ($token && $token->type == 'request' && $consumer) {
    $token->authorized = TRUE;
    $token->oauth_verifier = $oauth_verifier;
    $token->write(TRUE);
    // Get access token
    if ($access_token = trademe_api_create_access_token($consumer, $token, $oauth_verifier)) {
      drupal_set_message(t('Success! You’re now linked to your Trade Me account'));
    }
    else {
      // Error getting an access token
      watchdog('trademe_api', 'Error getting an access token, token: %token, type: %type', array('%token' => $token->key, '%type' => $token->type), WATCHDOG_ERROR);
      drupal_set_message(t('Access denied. You’re Trade Me account has not been linked.'), 'error');
    }
  }
  else {
    // API faliure
    watchdog('trademe_api', 'API faliure, token: %token, ', array('%token' => $token->key), WATCHDOG_ERROR);
    drupal_set_message(t('Access denied. You’re Trade Me account has not been linked.'), 'error');
  }

  // Redirect back to the application
  drupal_goto(variable_get('trademe_api_advanced_application_url', url('<front>', array('absolute' => TRUE))));
}

/*
 * This callback should just generate a request token and then redirect the user to the right TradeMe URL
 * to authorize. Created because generating the request token on page load isn't ideal
 */
function trademe_api_authorize_user_redirect() {
  global $user;

  // get the consumer token
  $consumer_token = variable_get('trademe_api_tokens_consumer_key', '');
  // load the consumer object
  $consumer = oauth_common_consumer_consumer_load($consumer_token);

  // Could not use the function oauth_common_get_request_token() as it sets
  // the default callback to 'oob' (out of band), and instead we want them
  // to be redirected back to the consumer application
  $request_token = trademe_api_create_request_token($consumer, $user);
  $trademe_authorisation_url = trademe_api_authorize_url($consumer, $request_token);

  // redirect user to the authorisation URL
  drupal_goto($trademe_authorisation_url);
}
